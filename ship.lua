local Ship = {}
Ship.__index = Ship

local keyInTable = function( t, key )
    for k, v in pairs( t ) do
        if k == key then
            return true
        end
    end
    return false
end

function Ship:init( x, y, runSpeed )
    local obj = {}
    setmetatable( obj, Ship )
    
    obj._keyDirections = {
        w = function( isReleased ) self:velocityUpdate( "y", -1, isReleased ); end,
        a = function( isReleased ) self:velocityUpdate( "x", -1, isReleased ); end,
        s = function( isReleased ) self:velocityUpdate( "y", 1, isReleased ); end,
        d = function( isReleased ) self:velocityUpdate( "x", 1, isReleased ); end,
    }
    
    obj._joyDirections = {
        dpup    = function( isReleased ) self:velocityUpdate( "y", -1, isReleased ); end,
        dpleft  = function( isReleased ) self:velocityUpdate( "x", -1, isReleased ); end,
        dpdown  = function( isReleased ) self:velocityUpdate( "y", 1, isReleased ); end,
        dpright = function( isReleased ) self:velocityUpdate( "x", 1, isReleased ); end,
    }
    
    obj.runSpeed = runSpeed
    obj.keyPress = ""
    obj.buttonPress = ""
    obj.xVelocity, obj.yVelocity = 0, 0
    obj.xPos, obj.yPos = x, y
    
    return obj
end

function Ship:velocityUpdate( direction, val, isReleased )
    local s = self.runSpeed
    print( isReleased )
    if isReleased then s = 0; end
    
    if direction == "y" then
        self.yVelocity = val * s
    else
        self.xVelocity = val * s
    end
 end


function Ship:load()
    -- Store the image data on a variable for faster lookup.
    -- newImage is NOT to be repeatedly executed!
   self.image = love.graphics.newImage("images/ship.png") 
end

function Ship:draw()
    -- Draw the ship on-screen
    love.graphics.draw( self.image, self.xPos, self.yPos )
end

function Ship:onPress( press )
    if keyInTable( press, "key" ) then
        if keyInTable( self._keyDirections, press.key ) then self:keyMove( press.key ); end
    end
    if keyInTable( press, "button" ) then
        if keyInTable( self._joyDirections, press.button ) then self:joyMove( press.button ); end
    end
end

function Ship:onRelease( press )
    if keyInTable( press, "key" ) then
        if keyInTable( self._keyDirections, press.key ) then self:keyMove( press.key, 1 ); end
    end
    if keyInTable( press, "button" ) then
        if keyInTable( self._joyDirections, press.button ) then self:joyMove( press.button, 1 ); end
    end
end

function Ship:keyMove( key, isReleased )
    -- This can be cleaned
    if key == "w" then self:velocityUpdate( "y", -1, isReleased )
    elseif key == "s" then self:velocityUpdate( "y", 1, isReleased )
    elseif key == "a" then self:velocityUpdate( "x", -1, isReleased )
    elseif key == "d" then self:velocityUpdate( "x", 1, isReleased )
    end
end

function Ship:joyMove( button, isReleased )
    -- This can be cleaned
    if key == "dpup" then self:velocityUpdate( "y", -1, isReleased )
    elseif key == "dpdown" then self:velocityUpdate( "y", 1, isReleased )
    elseif key == "dpleft" then self:velocityUpdate( "x", -1, isReleased )
    elseif key == "dpright" then self:velocityUpdate( "x", 1, isReleased )
    end
end

function Ship:update( dt )    
    self.xPos = self.xPos + (self.xVelocity * dt)
    self.yPos = self.yPos + (self.yVelocity * dt)
end

return Ship