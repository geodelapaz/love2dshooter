-- My first project in Love2d

----------------------------------------
-- Localizations and Forward-references

local SHIP_FACTORY = require( "ship" )
local ship
----------------------------------------
-- Initializer.
-- Loaded only onces used for initializing variables and stuff.
function love.load()
    ship = SHIP_FACTORY:init( 200, 200, 300 )
    ship:load()
end

----------------------------------------
-- Where drawing happens.
-- Calls to love.graphics.draw API should be performed here.
-- This is called repeatedly with each frame update
-- This means all interface and graphical changes should occur here
function love.draw()
    ship:draw()
end



----------------------------------------
-- Runtime.
-- Most computations should be performed here
-- as this is called repeatedly per frame update.
--
-- @param   double  dt  Delta time. Amount of 
--                      seconds since the last
--                      time this was called.
function love.update( dt )
    ship:update( dt )
end

----------------------------------------
-- Listens for keypresses
--
-- @param   string  key     the key pressed
--
function love.keypressed(key)
    ship:onPress( {key = key} )
end

function love.keyreleased(key)
    print( key )
    if key == "escape" then
        love.event.push("q")  -- actually causes the app to quit
        
        return;
    else
        ship:onRelease( {key = key} )
    end
end

----------------------------------------
-- Listens for Joystick events
function love.gamepadpressed(joystick, button)
    ship:onPress( {button = button} )
end

function love.gamepadreleased(joystick, button)
    ship:onRelease( {button = button} )
end
